{
  description = "michzappa.com";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOs/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, ... }@inputs:
    with inputs;
    (flake-utils.lib.eachDefaultSystem (system:
      let pkgs = (import nixpkgs { inherit system; });
      in rec {
        packages.website = pkgs.stdenv.mkDerivation {
          name = "michzappa.com";
          src = self;
          buildInputs =
            [ (pkgs.emacsWithPackages (epkgs: (with epkgs; [ htmlize ]))) ];
          buildPhase = "emacs -Q --script build.el";
          installPhase = ''
            cp -r public $out
          '';
        };
        defaultPackage = self.packages.${system}.website;
      }));
}
