;; this file is used in the context of `nix build'

;; dependencies
(require 'htmlize)
(require 'ox-publish)

(setq org-export-date-timestamp-format "%Y %m %d"
      org-publish-timestamp-directory "$out/org-timestamps"
      org-publish-project-alist
      `(("michzappa.com" :components ("org" "static"))
        ("org"
         :base-directory "./"
         :base-extension "org"
         :publishing-directory "./public"
         :publishing-function org-html-publish-to-html
         :recursive t
         :section-numbers nil
         :time-stamp-file nil
         :with-author nil
         :with-creator t
         :with-title nil
         :with-toc nil)
        ("static"
         :base-directory "./"
         :base-extension "css\\|txt\\|jpg\\|gif\\|png"
         :publishing-directory "./public"
         :publishing-function org-publish-attachment
         :recursive t))
      ;; org-html-head "<link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.min.css\" />"
      org-html-head-include-default-style nil
      org-html-head-include-scripts nil
      org-html-validation-link nil)

(org-publish-all t)
